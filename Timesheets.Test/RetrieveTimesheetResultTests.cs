﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Timesheets.Service;
using Timesheets.Service.Model;

namespace Timesheets.Tests
{
    [TestClass]
    public class RetrieveTimesheetResultTests
    {
        [TestMethod]
        public void RetrieveTimesheetResults_AllHoursAre4_ExpectedTotalHours28()
        {
            var staffMemberService = new StaffMemberService();
            var timesheetService = new TimesheetService();

            var staffMember = new StaffMemberModel
            {
                Rate = 25
            };

            var createdStaffMember = staffMemberService.Create(staffMember);

            var input = new TimesheetModel()
            {
                StaffMemberId = createdStaffMember.Id,
                TimesheetEntries = new List<TimesheetEntryModel>
                {
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Monday,
                        Hours = 4.0
                    },
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Tuesday,
                        Hours = 4.0
                    },
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Wednesday,
                        Hours = 4.0
                    },
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Thursday,
                        Hours = 4.0
                    },
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Friday,
                        Hours = 4.0
                    },
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Saturday,
                        Hours = 4.0
                    },
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Sunday,
                        Hours = 4.0
                    }
                }
            };
            List<TimesheetEntryModel> createdTimesheetEntries = timesheetService.Create(input);

            var result = timesheetService.GetTimesheetResults();
            var resultForStaffMember = result.Results.FirstOrDefault(tr => tr.StaffMemberId == createdStaffMember.Id);
            Assert.AreEqual(28, resultForStaffMember.TotalHours);
        }

        [DataTestMethod]
        [DataRow(25, 0, 0, 0, 0, 0, 4, 0, 101.5, DisplayName = "RetrieveTimesheetResults_RateIs25SaturdayHoursAre4_TotalCost101.5")]
        [DataRow(25, 0, 0, 0, 0, 0, 0, 4, 102, DisplayName = "RetrieveTimesheetResults_RateIs25SundayHoursAre4_TotalCost102")]
        [DataRow(25, 4, 4, 4, 4, 4, 4, 4, 703.5, DisplayName = "RetrieveTimesheetResults_RateIs25AllHoursAre4_TotalCost703.5")]
        [DataRow(50, 0, 0, 0, 0, 0, 1, 0, 50, DisplayName = "RetrieveTimesheetResults_RateIs50SaturdayHoursAre1_TotalCost50")]
        [DataRow(50, 0, 0, 0, 0, 0, 0, 1, 50, DisplayName = "RetrieveTimesheetResults_RateIs50SundayHoursAre1_TotalCost50")]
        [DataRow(49.5, 1, 1, 1, 1, 1, 1, 1, 347.5, DisplayName = "RetrieveTimesheetResults_RateIs49.5AllHoursAre1_TotalCost347.5")]
        public void RetrieveTimesheetResults_VerifyTotalCost(double rate, double mondayHours, double tuesdayHours, double wednesdayHours, double thursdayHours, double fridayHours, double saturdayHours, double sundayHours, double expectedTotalCost)
        {
            var staffMemberService = new StaffMemberService();
            var timesheetService = new TimesheetService();

            var staffMember = new StaffMemberModel
            {
                Rate = rate
            };

            var createdStaffMember = staffMemberService.Create(staffMember);

            var input = new TimesheetModel()
            {
                StaffMemberId = createdStaffMember.Id,
                TimesheetEntries = new List<TimesheetEntryModel>
                {
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Monday,
                        Hours = mondayHours
                    },
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Tuesday,
                        Hours = tuesdayHours
                    },
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Wednesday,
                        Hours = wednesdayHours
                    },
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Thursday,
                        Hours = thursdayHours
                    },
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Friday,
                        Hours = fridayHours
                    },
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Saturday,
                        Hours = saturdayHours
                    },
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Sunday,
                        Hours = sundayHours
                    }
                }
            };
            List<TimesheetEntryModel> createdTimesheetEntries = timesheetService.Create(input);

            var result = timesheetService.GetTimesheetResults();
            var resultForStaffMember = result.Results.FirstOrDefault(tr => tr.StaffMemberId == createdStaffMember.Id);
            Assert.AreEqual(expectedTotalCost, resultForStaffMember.TotalCost);
        }
    }
}
