﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Timesheets.Service;
using Timesheets.Service.Model;

namespace Timesheets.Tests
{
    [TestClass]
    public class RecordTimesheetTests
    {
        [TestMethod]
        public void CreateTimesheet_ValidRate_IsSuccessful()
        {
            var staffMemberService = new StaffMemberService();
            var timesheetService = new TimesheetService();

            var staffMember = new StaffMemberModel
            {
                Rate = 25
            };

            var createdStaffMember = staffMemberService.Create(staffMember);

            var input = new TimesheetModel()
            {
                StaffMemberId = createdStaffMember.Id,
                TimesheetEntries = new List<TimesheetEntryModel>
                {
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Monday,
                        Hours = 8.0
                    },
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Tuesday,
                        Hours = 8.0
                    },
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Wednesday,
                        Hours = 8.0
                    },
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Thursday,
                        Hours = 8.0
                    },
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Friday,
                        Hours = 8.0
                    },
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Saturday,
                        Hours = 8.0
                    },
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Sunday,
                        Hours = 8.0
                    }
                }
            };
            List<TimesheetEntryModel> createdTimesheetEntries = timesheetService.Create(input);

            foreach (var timesheetEntry in input.TimesheetEntries)
            {
                var createdTimesheetEntry = createdTimesheetEntries.SingleOrDefault(t => t.DayOfWeek == timesheetEntry.DayOfWeek);
                Assert.IsNotNull(createdTimesheetEntry);
                Assert.AreNotEqual(default(Guid), createdTimesheetEntry.Id);
                Assert.AreEqual(timesheetEntry.Hours, createdTimesheetEntry.Hours);
                Assert.AreEqual(input.StaffMemberId, createdTimesheetEntry.StaffMemberId);
            }
        }

        [TestMethod]
        public void CreateTimesheet_MissingDayOfWeek_IsException()
        {
            var staffMemberService = new StaffMemberService();
            var timesheetService = new TimesheetService();

            var staffMember = new StaffMemberModel
            {
                Rate = 25
            };

            var createdStaffMember = staffMemberService.Create(staffMember);

            var input = new TimesheetModel()
            {
                StaffMemberId = staffMember.Id,
                TimesheetEntries = new List<TimesheetEntryModel>
                {
                    // monday is missing for this test
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Tuesday,
                        Hours = 8.0
                    },
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Wednesday,
                        Hours = 8.0
                    },
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Thursday,
                        Hours = 8.0
                    },
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Friday,
                        Hours = 8.0
                    },
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Saturday,
                        Hours = 8.0
                    },
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Sunday,
                        Hours = 8.0
                    }
                }
            };

            Assert.ThrowsException<Exception>(() =>
            {
                timesheetService.Create(input);
            });
        }

        [TestMethod]
        public void CreateTimesheet_DoubleEntryDayOfWeek_IsException()
        {
            var staffMemberService = new StaffMemberService();
            var timesheetService = new TimesheetService();

            var staffMember = new StaffMemberModel
            {
                Rate = 25
            };

            var createdStaffMember = staffMemberService.Create(staffMember);

            var input = new TimesheetModel()
            {
                StaffMemberId = staffMember.Id,
                TimesheetEntries = new List<TimesheetEntryModel>
                {
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Monday,
                        Hours = 8.0
                    },
                    // monday is twice for this test
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Monday,
                        Hours = 8.0
                    },
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Tuesday,
                        Hours = 8.0
                    },
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Wednesday,
                        Hours = 8.0
                    },
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Thursday,
                        Hours = 8.0
                    },
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Friday,
                        Hours = 8.0
                    },
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Saturday,
                        Hours = 8.0
                    },
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Sunday,
                        Hours = 8.0
                    }
                }
            };

            Assert.ThrowsException<Exception>(() =>
            {
                timesheetService.Create(input);
            });
        }

        [DataTestMethod]
        [DataRow(-0.01, DisplayName = "CreateTimesheet_HoursLessThan0_IsException")]
        [DataRow(24.01, DisplayName = "CreateTimesheet_HoursGreaterThan24_IsException")]
        public void CreateTimesheet_HoursOutOfRange_IsException(double hours)
        {
            var staffMemberService = new StaffMemberService();
            var timesheetService = new TimesheetService();

            var staffMember = new StaffMemberModel
            {
                Rate = 25
            };

            var createdStaffMember = staffMemberService.Create(staffMember);

            var input = new TimesheetModel()
            {
                StaffMemberId = staffMember.Id,
                TimesheetEntries = new List<TimesheetEntryModel>
                {
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Monday,
                        Hours = hours
                    },
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Tuesday,
                        Hours = 8.0
                    },
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Wednesday,
                        Hours = 8.0
                    },
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Thursday,
                        Hours = 8.0
                    },
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Friday,
                        Hours = 8.0
                    },
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Saturday,
                        Hours = 8.0
                    },
                    new TimesheetEntryModel
                    {
                        DayOfWeek = DayOfWeek.Sunday,
                        Hours = 8.0
                    }
                }
            };

            Assert.ThrowsException<Exception>(() =>
            {
                timesheetService.Create(input);
            });
        }
    }
}
