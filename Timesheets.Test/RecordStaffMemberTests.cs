﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Timesheets.Service;
using Timesheets.Service.Model;

namespace Timesheets.Tests
{
    [TestClass]
    public class RecordStaffMemberTests
    {
        [DataTestMethod]
        [DataRow(0.01, DisplayName = "CreateStaffMember_RateEquals0.01_IsSuccessful")]
        [DataRow(25.0, DisplayName = "CreateStaffMember_RateEquals25_IsSuccessful")]
        [DataRow(50, DisplayName = "CreateStaffMember_RateEquals50_IsSuccessful")]
        public void CreateStaffMember_ValidRate_IsSuccessful(double rate)
        {
            var staffMemberService = new StaffMemberService();

            var input = new StaffMemberModel()
            {
                Rate = rate
            };
            var createdStaffMember = staffMemberService.Create(input);

            // Check staff member has assigned Id.
            Assert.AreNotEqual(default(Guid), createdStaffMember.Id);

            // Check staff member Rate
            Assert.AreEqual(input.Rate, createdStaffMember.Rate);
        }

        [DataTestMethod]
        [DataRow(-0.01, DisplayName = "CreateStaffMember_RateEquals-0.01_IsException")]
        [DataRow(0, DisplayName = "CreateStaffMember_RateEquals0_IsException")]
        [DataRow(50.01, DisplayName = "CreateStaffMember_RateEquals50.01_IsException")]
        public void CreateStaffMember_InvalidRate_IsException(double rate)
        {
            var staffMemberService = new StaffMemberService();

            var input = new StaffMemberModel()
            {
                Rate = rate
            };
            Assert.ThrowsException<Exception>(() =>
            {
                staffMemberService.Create(input);
            });
        }
    }
}
