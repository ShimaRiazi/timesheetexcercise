﻿using System;
using System.Collections.Generic;
using System.Linq;
using Timesheets.Data.Entity;
using Timesheets.Data.Repository;
using Timesheets.Service.Model;

namespace Timesheets.Service
{
    public class TimesheetService
    {
        private readonly StaffMemberRepository _staffMemberRepository;
        private readonly TimesheetEntryRepository _timesheetEntryRepository;

        private readonly Dictionary<DayOfWeek, double> _penaltyRates;

        public TimesheetService()
        {
            _staffMemberRepository = new StaffMemberRepository();
            _timesheetEntryRepository = new TimesheetEntryRepository();

            _penaltyRates = new Dictionary<DayOfWeek, double>
            {
                { DayOfWeek.Saturday, 0.015 },
                { DayOfWeek.Sunday, 0.02 }
            };
        }

        public TimesheetResults GetTimesheetResults()
        {
            // get all staff members
            var staffMembers = _staffMemberRepository.ListEntities();

            // get all timesheets
            var timesheets = _timesheetEntryRepository.ListEntities();

            // calculate totals
            var results = new TimesheetResults();
            foreach (var staffMember in staffMembers)
            {
                var result = new TimesheetResult()
                {
                    StaffMemberId = staffMember.Id,
                    TotalHours = 0,
                    TotalCost = 0
                };
                var staffMemberTimesheets = timesheets.Where(t => t.StaffMemberId == staffMember.Id);
                foreach (var timesheet in staffMemberTimesheets)
                {
                    var rateModifier = 1.0;
                    if (_penaltyRates.ContainsKey(timesheet.DayOfWeek))
                    {
                        rateModifier += _penaltyRates[timesheet.DayOfWeek];
                    }
                    result.TotalHours += timesheet.Hours;
                    var modifiedRate = staffMember.Rate * rateModifier;
                    if (modifiedRate > 50)
                    {
                        modifiedRate = 50;
                    }
                    result.TotalCost += Math.Round(timesheet.Hours * modifiedRate, 2);
                }
                results.Results.Add(result);
            }

            return results;
        }

        public List<TimesheetEntryModel> Create(TimesheetModel input)
        {
            // validate model
            // validate daysofweek
            var inputDaysOfWeek = input.TimesheetEntries.Select(tem => tem.DayOfWeek);
            var expectedDaysOfWeek = (DayOfWeek[]) Enum.GetValues(typeof(DayOfWeek));
            if (inputDaysOfWeek.Count() != expectedDaysOfWeek.Count() || !expectedDaysOfWeek.All(d => inputDaysOfWeek.Contains(d)))
            {
                throw new Exception("Timesheet days of week are invalid.");
            }

            // validate hours
            if (input.TimesheetEntries.Any(tem => tem.Hours > 24 || tem.Hours < 0))
            {
                throw new Exception("Timesheet hours are invalid.");
            }

            foreach (var timesheetEntry in input.TimesheetEntries)
            {
                timesheetEntry.StaffMemberId = input.StaffMemberId;
                var createdEntity = _timesheetEntryRepository.CreateEntity(new TimesheetEntry
                {
                    DayOfWeek = timesheetEntry.DayOfWeek,
                    Hours = timesheetEntry.Hours,
                    StaffMemberId = timesheetEntry.StaffMemberId
                });
                timesheetEntry.Id = createdEntity.Id;
            }
            return input.TimesheetEntries;
        }
    }
}
