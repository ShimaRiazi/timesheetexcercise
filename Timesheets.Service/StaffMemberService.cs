﻿using System;
using Timesheets.Data.Entity;
using Timesheets.Data.Repository;
using Timesheets.Service.Model;

namespace Timesheets.Service
{
    public class StaffMemberService
    {
        private readonly StaffMemberRepository _staffMemberRepository;

        public StaffMemberService()
        {
            _staffMemberRepository = new StaffMemberRepository();
        }

        public StaffMemberModel Create(StaffMemberModel staffMemberModel)
        {
            // validate staffMemberModel
            if (staffMemberModel.Rate > 50 || staffMemberModel.Rate <= 0)
            {
                throw new Exception("Invalid Rate");
            }

            var staffMemberEntity = new StaffMember
            {
                Rate = staffMemberModel.Rate
            };

            var createdStaffMember = _staffMemberRepository.CreateEntity(staffMemberEntity);
            return new StaffMemberModel
            {
                Id = createdStaffMember.Id,
                Rate = createdStaffMember.Rate
            };
        }

        public StaffMemberModel Get(Guid id)
        {
            throw new NotImplementedException();
        }
    }
}