﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Timesheets.Data.Entity;

namespace Timesheets.Service.Model
{
    public class TimesheetResults
    {
        public List<TimesheetResult> Results { get; set; } = new List<TimesheetResult>();
    }

    public class TimesheetResult
    {
        public Guid StaffMemberId { get; set; }
        public double TotalHours { get; set; }
        public double TotalCost { get; set; }
    }
}
