﻿using System;
using System.Collections.Generic;

namespace Timesheets.Service.Model
{
    public class TimesheetModel
    {
        public TimesheetModel()
        {
        }

        public Guid StaffMemberId { get; set; }
        public List<TimesheetEntryModel> TimesheetEntries { get; set; }
    }
}