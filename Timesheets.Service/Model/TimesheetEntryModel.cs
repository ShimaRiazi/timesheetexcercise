﻿using System;

namespace Timesheets.Service.Model
{
    public class TimesheetEntryModel
    {
        public Guid Id { get; set; }
        public Guid StaffMemberId { get; set; }
        public DayOfWeek DayOfWeek { get; set; }
        public double Hours { get; set; }
    }
}