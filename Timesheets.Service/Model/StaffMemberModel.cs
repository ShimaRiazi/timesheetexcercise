﻿using System;

namespace Timesheets.Service.Model
{
    public class StaffMemberModel
    {
        public Guid Id { get; set; }
        public double Rate { get; set; }
    }
}