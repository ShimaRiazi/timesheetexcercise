﻿using System;
using System.Collections.Generic;
using Timesheets.Data.Entity;

namespace Timesheets.Data.Repository
{
    public abstract class BaseRepository<TEntity> where TEntity : IEntity
    {
        private static Dictionary<Guid, TEntity> Entities = new Dictionary<Guid, TEntity>();

        public TEntity CreateEntity(TEntity entity)
        {
            entity.Id = Guid.NewGuid();
            Entities.Add(entity.Id, entity);
            return entity;
        }

        public TEntity ReadEntity(Guid id)
        {
            return Entities[id];
        }

        public void UpdateEntity(TEntity entity)
        {
            Entities[entity.Id] = entity;
        }

        public void DeleteEntity(Guid id)
        {
            Entities.Remove(id);
        }

        public IEnumerable<TEntity> ListEntities()
        {
            return Entities.Values;
        }
    }
}
