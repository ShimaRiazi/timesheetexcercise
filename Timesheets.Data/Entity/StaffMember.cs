﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timesheets.Data.Entity
{
    public class StaffMember : IEntity
    {
        public Guid Id { get; set; }
        public double Rate { get; set; }
    }
}
