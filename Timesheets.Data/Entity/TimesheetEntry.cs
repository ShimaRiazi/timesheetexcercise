﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timesheets.Data.Entity
{
    public class TimesheetEntry : IEntity
    {
        public Guid Id { get; set; }
        public Guid StaffMemberId { get; set; }
        public double Hours { get; set; }
        public DayOfWeek DayOfWeek { get; set; }
    }
}
